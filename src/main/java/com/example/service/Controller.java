package com.example.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Value("${valami}")
    String appname;

    @GetMapping("/helo")
    public String helo(){
        return "HELO from " + appname;
    }
}
